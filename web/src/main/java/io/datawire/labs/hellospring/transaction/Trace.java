package io.datawire.labs.hellospring.transaction;

public class Trace {
    public String uniqueName;
    public String action;
    public String dataSpec;
    public String time;
}
