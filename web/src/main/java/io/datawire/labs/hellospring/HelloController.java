package io.datawire.labs.hellospring;

import java.util.concurrent.TimeUnit;

import io.datawire.labs.hellospring.request.Identity;
import io.datawire.labs.hellospring.request.MyData;
import io.datawire.labs.hellospring.request.Rule;
import io.datawire.labs.hellospring.request.RuleContent;
import io.datawire.labs.hellospring.request.TheirData;
import io.datawire.labs.hellospring.transaction.Key;
import io.datawire.labs.hellospring.transaction.Setting;
import io.datawire.labs.hellospring.transaction.User;
import io.datawire.labs.hellospring.w3j.Application;
import io.datawire.labs.hellospring.w3j.IsApplication;
import io.datawire.labs.hellospring.w3j.PsApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    static final String REJECT = "REJECT";
    static final String ACCEPT = "ACCEPT";
    private static long start = System.currentTimeMillis();

    static Logger log = LoggerFactory.getLogger(IsApplication.class);

    @GetMapping("/")
    public String sayHello() {
        long millis = System.currentTimeMillis() - start;
        String uptime = String.format("%02d:%02d",
                                      TimeUnit.MILLISECONDS.toMinutes(millis),
                                      TimeUnit.MILLISECONDS.toSeconds(millis) -
                                      TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        return String.format("Hello, Spring! (up %s, %s)", uptime, System.getenv("BUILD_PROFILE"));
    }

    @GetMapping("/tx")
    public String getTransaction() {
        log.info("tx");
        String ret = "";
        try {
            ret = Application.getTransaction(null);
        } catch (Exception e) {
            ret = e.getMessage();
        }
        return ret;
    }

    @PostMapping("/is/identity")
    public ResponseEntity<?> postIsIdentity(@RequestBody Identity identity) {
        log.info("/is/identity");
        String ret = ACCEPT;
        if (identity == null || identity.token == null) {
            return new ResponseEntity<>(REJECT, HttpStatus.UNAUTHORIZED);
        }
        if (identity.identityKey == null || identity.startTime == null || identity.endTime == null) {
            return new ResponseEntity<>(REJECT, HttpStatus.BAD_REQUEST);
        }
        if (identity.startTime.after(identity.endTime)) {
            return new ResponseEntity<>(REJECT, HttpStatus.FORBIDDEN);
        }
        User user = new User();
        user.uniqueName = identity.token;
        user.displayName = identity.token;
        user.publicKey = identity.identityKey;
        user.startTime = identity.startTime.toString();
        user.endTime = identity.endTime.toString();

        IsApplication app = new IsApplication();
        ret = app.doIdentity(user);
        return new ResponseEntity<>(ret, HttpStatus.OK);
    }

    @PostMapping("/is/rule")
    public ResponseEntity<?> postIsRule(@RequestBody Rule rule) {
        log.info("/is/rule");
        String ret = ACCEPT;
        if (rule == null || rule.token == null) {
            return new ResponseEntity<>(REJECT, HttpStatus.UNAUTHORIZED);
        }
        if (rule.startTime == null || rule.endTime == null) {
            return new ResponseEntity<>(REJECT, HttpStatus.BAD_REQUEST);
        }
        if (rule.startTime.after(rule.endTime)) {
            return new ResponseEntity<>(REJECT, HttpStatus.FORBIDDEN);
        }
        Setting setting = new Setting();
        setting.uniqueName = rule.token;
        setting.startTime = rule.startTime.toString();
        setting.endTime = rule.endTime.toString();
        setting.data = rule.rule;
        IsApplication app = new IsApplication();
        ret = app.doRule(setting);
        return new ResponseEntity<>(ret, HttpStatus.OK);
    }

    @GetMapping("/ps/rulesample")
    public ResponseEntity<?> getPsRuleSample() {
        log.info("/ps/rulesample");
        String ret = "";
        HttpStatus code = HttpStatus.OK;
        try {
            ret = Application.getTransaction(null);
        } catch (Exception e) {
            ret = e.getMessage();
            code = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(ret, code);
    }

    @PostMapping("/ps/rulecontent")
    public ResponseEntity<?> postPsRuleContent(@RequestBody RuleContent ruleContent) {
        log.info("/ps/rulecontent");
        String ret = ACCEPT;
        if (ruleContent == null || ruleContent.token == null) {
            return new ResponseEntity<>(REJECT, HttpStatus.UNAUTHORIZED);
        }
        if (ruleContent.pos == null || ruleContent.rule == null) {
            return new ResponseEntity<>(REJECT, HttpStatus.BAD_REQUEST);
        }
        if (ruleContent.privateKey == null || ruleContent.behaviorKey == null) {
            return new ResponseEntity<>(REJECT, HttpStatus.BAD_REQUEST);
        }
        Key key = new Key();
        key.uniqueName = ruleContent.token;
        key.privateKey = ruleContent.privateKey;
        key.behaviorKey = ruleContent.behaviorKey;

        PsApplication app = new PsApplication();
        ret = app.doRuleContent(key);
        return new ResponseEntity<>(ret, HttpStatus.OK);
    }

    @GetMapping("/ps/rulecontent")
    public ResponseEntity<?>  getPsRuleContent(@RequestParam(name = "from") String from, @RequestParam(name = "to") String to) {
        log.info("/ps/rulecontent");
        String ret = REJECT;
        HttpStatus code = HttpStatus.OK;
        if (from == null || to == null) {
            return new ResponseEntity<>(ret, HttpStatus.BAD_REQUEST);
        }
        try {
            PsApplication app = new PsApplication();
            ret = app.getRuleContent("STB");
        } catch (Exception e) {
            ret = e.getMessage();
            code = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(ret, code);
    }

    @PostMapping("/ss/mydata")
    public ResponseEntity<?> postSsMyData(@RequestBody MyData myData) {
        log.info("/ss/mydata");
        String ret = ACCEPT;
        if (myData == null || myData.token == null) {
            return new ResponseEntity<>(REJECT, HttpStatus.UNAUTHORIZED);
        }
        if (myData.time == null || myData.data == null) {
            return new ResponseEntity<>(REJECT, HttpStatus.BAD_REQUEST);
        }
        try {
            Application app = new Application();
            ret = app.saveFile(myData);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(ret, HttpStatus.OK);
    }

    @PostMapping("/ss/theirdata")
    public ResponseEntity<?> postSsTheirData(@RequestBody TheirData theirData) {
        log.info("/ss/theirdata");
        String ret = ACCEPT;
        if (theirData == null || theirData.token == null) {
            return new ResponseEntity<>(REJECT, HttpStatus.UNAUTHORIZED);
        }
        if (theirData.from == null || theirData.to == null) {
            return new ResponseEntity<>(REJECT, HttpStatus.BAD_REQUEST);
        }
        try {
            Application app = new Application();
            ret = app.loadFile(theirData);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(ret, HttpStatus.OK);
    }
}
