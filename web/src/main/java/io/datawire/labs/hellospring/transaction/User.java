package io.datawire.labs.hellospring.transaction;

public class User {
    public String uniqueName;
    public String displayName;
    public String publicKey;
    public String legal;
    public String previous;
    public String startTime;
    public String endTime;
}
