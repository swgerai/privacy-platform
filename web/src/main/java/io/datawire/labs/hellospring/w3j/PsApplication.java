package io.datawire.labs.hellospring.w3j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.utils.Convert;

import java.util.HashMap;
import java.util.Map;

import io.datawire.labs.hellospring.transaction.Key;
import io.datawire.labs.hellospring.transaction.Setting;
import io.datawire.labs.hellospring.transaction.User;
import io.datawire.labs.hellospring.util.BcHelper;
import io.datawire.labs.hellospring.util.JsonHelper;

public class PsApplication {

    Logger log = LoggerFactory.getLogger(PsApplication.class);

    static Map<String, String> CACHEDTX = new HashMap<>();

    private String getLatestTx(String id) {
        String tx = "";
        tx = queryCachedTx(id);
        if ("".equals(tx)) {
            tx = queryLatestTx(id);
        }

        return tx;
    }

    private String queryLatestTx(String id) {
        String tx = "";
        // Query to BC network
        return tx;
    }

    private String queryCachedTx(String id) {
        return CACHEDTX.getOrDefault(id, "");
    }

    private void setCachedTx(String id, String tx) {
        CACHEDTX.put(id, tx);
    }

    private String putTransaction(Object obj) throws Exception {
        String tx = "";
        log.error("start of Application run");
        // We start by creating a new web3j instance to connect to remote nodes on the network.
        // Note: if using web3j Android, use Web3jFactory.build(...
        Web3j web3j = Web3j.build(new HttpService(
                "https://ropsten.infura.io/v3/ed0506f5cf38481c9d7340bee5673ada"));
        log.info("Connected to Ethereum client version: "
                + web3j.web3ClientVersion().send().getWeb3ClientVersion());

        // We then need to load our Ethereum wallet file
        Credentials credentials =
                WalletUtils.loadCredentials(
                        "Aa@123456",
                        "UTC--2021-07-21T07-34-23.229000000Z--146f190f3248d206b2272503c65cbbf82ff30158.json");
        log.info("Credentials loaded");

        log.info("Sending 1 Wei ("
                + Convert.fromWei("1", Convert.Unit.ETHER).toPlainString() + " Ether) with DATA");
        String data = JsonHelper.toJsonString(obj);
        log.error(data);
        TransactionReceipt transferReceipt = MyTransfer.sendData(
                web3j,
                credentials,
                "0xfee7eefcdf2df2530c03658837bcc0e0c30cfd25",  // you can put any address here
                BcHelper.ASCIItoHEX(data))  // data to send
                .send();
        tx = transferReceipt.getTransactionHash();
        log.info("Transaction complete, view it at https://ropsten.etherscan.io/tx/"
                + tx);
        return tx;
    }

    public String doRuleContent(Key key) {
        String tx = "";

        try {
            tx = putTransaction(key);
            setCachedTx(key.uniqueName, tx);
        } catch (Exception e) {
            // do nothing
        }

        return tx;
    }

    public String getRuleContent(String token) {
        String content ="";
        String tx = getLatestTx(token);

        if (tx == null || tx.equals("")) {
            return content;
        }

        try {
            content = Application.getTransaction(tx);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return content;
    }
}
