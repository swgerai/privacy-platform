package io.datawire.labs.hellospring.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class RuleContent {
    public String token;
    public String pos;
    public String rule;
    public String privateKey;
    public String behaviorKey;

}
