package io.datawire.labs.hellospring.w3j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.Transaction;
import org.web3j.protocol.http.HttpService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Optional;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.datawire.labs.hellospring.request.MyData;
import io.datawire.labs.hellospring.request.TheirData;


public class Application {
    Logger log = LoggerFactory.getLogger(Application.class);

    private static Map<String, Set> TOKENS = new HashMap();

    public Application() {
        File folder = new File(".");
        File[] fileList = folder.listFiles();
        if (fileList != null) {
            log.error(fileList.length + "");
        } else {
            log.error("TOI");
        }
        Pattern pattern = Pattern.compile("([a-z]*?)_([0-9]*?).dat");
        for (File file : fileList) {
            Matcher matcher = pattern.matcher(file.getName());
            if (matcher.find()) {
                HashSet<String> files = (HashSet<String>) TOKENS.get(matcher.group(1));
                if (files == null) {
                    files = new HashSet<>();
                    TOKENS.put(matcher.group(1), files);
                }
                files.add(matcher.group(2));
            }
        }
    }

    public static String getTransaction(String tran) throws Exception {
        // We start by creating a new web3j instance to connect to remote nodes on the network.
        // Note: if using web3j Android, use Web3jFactory.build(...
        Web3j web3j = Web3j.build(new HttpService(
                "https://ropsten.infura.io/v3/ed0506f5cf38481c9d7340bee5673ada"));

        String transactionHash = "0xea35ead7acf4a53a511be4c9cfffe1ccfa3295c4b19b0f3e3bb936a07009c415";
        if (tran != null) {
            transactionHash = tran;
        }
        Optional<Transaction> transaction = web3j.ethGetTransactionByHash(transactionHash)
                                                 .send()
                                                 .getTransaction();
        String hexString = transaction.get().getInput();

        // Remove the prefix 0x
        if(hexString.startsWith("0x")) {
            hexString = hexString.substring(2);
        }

        // Convert Base16 into a byte array
        byte[] byteArray = new BigInteger(hexString, 16).toByteArray();
        return new String(byteArray);
    }

    public String saveFile(MyData data) throws Exception {
        String time = String.valueOf(data.time.getTime());

        File file = new File(data.token + "_" + time + ".dat");
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(data.data);
        bw.close();

        // Update data
        HashSet<String> files = (HashSet<String>) TOKENS.get(data.token);
        if (files == null) {
            files = new HashSet<>();
            TOKENS.put(data.token, files);
        }
        files.add(time);

        return time;
    }

    public String loadFile(TheirData data) throws Exception {
        String content = "";
        String from = String.valueOf(data.from.getTime());;
        String to = String.valueOf(data.to.getTime());;

        // Check privacy
        if (!"MDA".equals(data.token)) {
            return content;
        }

        Random rand = new Random();
        String allow = "behavior";
        if (rand.nextBoolean()) {
            allow = "private";
        }

        log.error("allow this time: " + allow);

        HashSet<String> files = (HashSet<String>) TOKENS.get(allow);
        if (files == null) {
            return content;
        }

        log.info("number of item: " + files.size());

        for (String time : files) {
            if (time.compareTo(from) < 0 || time.compareTo(to) > 0) {
                continue;
            }
            File file = new File(allow + "_" +time + ".dat");
            BufferedReader br = new BufferedReader(new FileReader(file));
//            content += time + ":\n";
            String line = br.readLine();
            while (line != null) {
                content += line;
                line = br.readLine();
            }
//            content += "\n============================\n\n";
            br.close();
        }

        return content;
    }
}
