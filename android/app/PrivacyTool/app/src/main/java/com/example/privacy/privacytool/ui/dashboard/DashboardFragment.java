package com.example.privacy.privacytool.ui.dashboard;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.privacy.privacytool.MainActivity;
import com.example.privacy.privacytool.R;
import com.example.privacy.privacytool.response.Key;
import com.example.privacy.privacytool.util.HttpHelper;
import com.example.privacy.privacytool.util.JsonHelper;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;

    private EditText editMyId;
    private EditText editAccessId;
    private EditText editKeys;
    private Button butRetrieve;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        MainActivity activity = (MainActivity) getActivity();
        dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        editMyId = root.findViewById(R.id.edit_dashboard_user);
        activity.myId = editMyId.getText().toString();
        editMyId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                activity.myId = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editAccessId = root.findViewById(R.id.edit_dashboard_access);
        activity.accessId = editAccessId.getText().toString();
        editAccessId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                activity.accessId = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editKeys = root.findViewById(R.id.edit_dashboard_access_key);
        editKeys.setText("behavior: " + activity.keyBehavior + "\n private: " + activity.keyPrivate);

        butRetrieve = root.findViewById(R.id.button_dashboard_access_key);
        butRetrieve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HttpHelper sender = new HttpHelper();
                try {
                    sender.doRuleContent();
                    String keys = sender.getResponse();
                    if (!"".equals(keys)) {
                        Key key = JsonHelper.fromJsonString(keys, Key.class);
                        activity.keyPrivate = key.privateKey;
                        activity.keyBehavior = key.behaviorKey;
                        editKeys.setText("behavior: " + activity.keyBehavior + "\n private: " + activity.keyPrivate);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return root;
    }
}