package com.example.privacy.privacytool.util;

import android.content.Context;
import android.util.Log;

import com.example.privacy.privacytool.request.MyData;
import com.example.privacy.privacytool.request.TheirData;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.concurrent.TimeoutException;

public class HttpHelper {
    private final String TAG = "RAI-PRIV";
    Context mContext;
    public Object mData;

    private String mResponse;

    public HttpHelper() {

    }

    public String getResponse() throws Exception {
        int count = 0;
        while (count < 5 || mResponse == null) {
            count ++;
            Thread.sleep(1000);
        }
        if (mResponse == null) {
            throw new TimeoutException();
        }
        return mResponse;
    }

    public void doSend(MyData data) throws Exception {
        mData = data;
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try  {
                    send((MyData) mData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

    }

    public void send(MyData data) throws Exception {
        URL url = new URL("http://10.0.2.2:8080/ss/mydata");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json; utf-8");
        connection.setRequestProperty("Accept", "application/json");
        connection.setDoOutput(true);
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);

        String att = JsonHelper.toJsonString(data);
        try(OutputStream os = connection.getOutputStream()) {
            byte[] input = att.getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        connection.connect();
    }

    public void doTransaction() throws Exception {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try  {
                    mResponse = getTransaction();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

    }

    private String getTransaction() throws Exception {
        String content = "";
        URL url = new URL("http://10.0.2.2:8080/tx");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        connection.connect();
        int responseCode = connection.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            Log.e(TAG, "HTTP: " + responseCode + "| " + connection.getResponseMessage());
            return content;
        }
        BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            content += line + "\n";
        }
        Log.e(TAG, content);

        return content;
    }

    public void doGet() throws Exception {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try  {
                    mResponse = getTheir();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

    }

    public void doRuleContent() throws Exception {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try  {
                    mResponse = getRuleContent();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

    }

    private String getRuleContent() throws Exception {
        String content = "";
        URL url = new URL("http://10.0.2.2:8080/ps/rulecontent?from=now&to=then");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        connection.connect();
        int responseCode = connection.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            Log.e(TAG, "HTTP: " + responseCode + "| " + connection.getResponseMessage());
            return content;
        }
        BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            content += line + "\n";
        }
        Log.e(TAG, content);

        return content;
    }

    public String getTheir() throws Exception {
        URL url = new URL("http://10.0.2.2:8080/ss/theirdata");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);

        TheirData request = new TheirData();
        request.token = "MDA";
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        request.from = df.parse("2010-01-01T00:00");
        request.to = new Date(System.currentTimeMillis());;

        String att = JsonHelper.toJsonString(request);
        try(OutputStream os = connection.getOutputStream()) {
            byte[] input = att.getBytes("utf-8");
            os.write(input, 0, input.length);
            os.flush();
        }
        connection.connect();
        String json_response = "";
        InputStreamReader in = new InputStreamReader(connection.getInputStream());
        BufferedReader br = new BufferedReader(in);
        String text = "";
        while ((text = br.readLine()) != null) {
            json_response += text + "\n";
        }

        Log.e("RAI-PRIV", json_response);
        return json_response;
    }
}
