package com.example.privacy.privacytool.w3j;

import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.exceptions.TransactionException;
import org.web3j.tx.ManagedTransaction;
import org.web3j.tx.RawTransactionManager;
import org.web3j.tx.TransactionManager;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

public class MyTransfer extends ManagedTransaction {
    public static final BigInteger GAS_LIMIT = BigInteger.valueOf(4_300_000);

    public MyTransfer(Web3j web3j, TransactionManager transactionManager) {
        super(web3j, transactionManager);
    }

    public static RemoteCall<TransactionReceipt> sendFunds(
            Web3j web3j,
            Credentials credentials,
            String toAddress,
            BigDecimal value,
            Convert.Unit unit)
            throws InterruptedException, IOException, TransactionException {

        TransactionManager transactionManager = new RawTransactionManager(web3j, credentials);

        return new RemoteCall<>(
                () -> new MyTransfer(web3j, transactionManager).send(toAddress, value, unit, ""));
    }

    public static RemoteCall<TransactionReceipt> sendData(
            Web3j web3j,
            Credentials credentials,
            String toAddress,
            String data)
            throws InterruptedException, IOException, TransactionException {

        TransactionManager transactionManager = new RawTransactionManager(web3j, credentials);

        return new RemoteCall<>(
                () -> new MyTransfer(web3j, transactionManager).send(toAddress, BigDecimal.ONE, Convert.Unit.WEI, data));
    }

    private TransactionReceipt send(String toAddress, BigDecimal value, Convert.Unit unit, String data)
            throws IOException, InterruptedException, TransactionException {

        BigInteger gasPrice = requestCurrentGasPrice();
        return send(toAddress, value, unit, gasPrice, GAS_LIMIT, data);
    }

    private TransactionReceipt send(
            String toAddress,
            BigDecimal value,
            Convert.Unit unit,
            BigInteger gasPrice,
            BigInteger gasLimit,
            String data)
            throws IOException, InterruptedException, TransactionException {

        BigDecimal weiValue = Convert.toWei(value, unit);
        if (!Numeric.isIntegerValue(weiValue)) {
            throw new UnsupportedOperationException(
                    "Non decimal Wei value provided: "
                            + value
                            + " "
                            + unit.toString()
                            + " = "
                            + weiValue
                            + " Wei");
        }

        String resolvedAddress = ensResolver.resolve(toAddress);
        return send(resolvedAddress, data, weiValue.toBigIntegerExact(), gasPrice, gasLimit);
    }
}
