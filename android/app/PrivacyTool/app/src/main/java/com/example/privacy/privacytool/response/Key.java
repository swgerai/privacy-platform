package com.example.privacy.privacytool.response;

public class Key {
    public String uniqueName;
    public String privateKey;
    public String behaviorKey;
}
