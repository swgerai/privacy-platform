package com.example.privacy.privacytool.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class JsonHelper {

    public static String toJsonString(Object o) {
        String out = "";
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            out = ow.writeValueAsString(o);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }

    public static <T> T fromJsonString(String s, Class<T> c) {
        Object out = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            out = mapper.readValue(s, c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T)out;
    }
}
