package com.example.privacy.privacytool.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.privacy.privacytool.util.HttpHelper;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public HomeViewModel() {
        mText = new MutableLiveData<>();
        HttpHelper sender = new HttpHelper();
        try {
            sender.doGet();
            mText.setValue(sender.getResponse());
        } catch (Exception e) {
            e.printStackTrace();
            mText.setValue("");
        }
    }

    public LiveData<String> getText() {
        return mText;
    }
}