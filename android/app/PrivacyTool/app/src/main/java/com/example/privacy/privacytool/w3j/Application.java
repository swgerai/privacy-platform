package com.example.privacy.privacytool.w3j;

import com.example.privacy.privacytool.request.MyData;
import com.example.privacy.privacytool.request.TheirData;

import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.Transaction;
import org.web3j.protocol.http.HttpService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class Application {

    private static Map<String, Set> TOKENS = new HashMap();
    public static String getTransaction() throws Exception {
        // We start by creating a new web3j instance to connect to remote nodes on the network.
        // Note: if using web3j Android, use Web3jFactory.build(...
        Web3j web3j = Web3j.build(new HttpService(
                "https://ropsten.infura.io/v3/ed0506f5cf38481c9d7340bee5673ada"));

        String transactionHash = "0xea35ead7acf4a53a511be4c9cfffe1ccfa3295c4b19b0f3e3bb936a07009c415";
        Optional<Transaction> transaction = web3j.ethGetTransactionByHash(transactionHash)
                                                 .send()
                                                 .getTransaction();
        String hexString = transaction.get().getInput();

        // Remove the prefix 0x
        if(hexString.startsWith("0x")) {
            hexString = hexString.substring(2);
        }

        // Convert Base16 into a byte array
        byte[] byteArray = new BigInteger(hexString, 16).toByteArray();
        return new String(byteArray);
    }

    public static String saveFile(MyData data) throws Exception {
        String time = String.valueOf(data.time.getTime());

        File file = new File(time + ".dat");
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(data.data);
        bw.close();

        // Update data
        HashSet<String> files = (HashSet<String>) TOKENS.get(data.token);
        if (files == null) {
            files = new HashSet<>();
            TOKENS.put(data.token, files);
        }
        files.add(time);

        return time;
    }

    public static String loadFile(TheirData data) throws Exception {
        String content = "";
        String from = String.valueOf(data.from.getTime());;
        String to = String.valueOf(data.to.getTime());;

        HashSet<String> files = (HashSet<String>) TOKENS.get(data.token);
        if (files == null) {
            return content;
        }

        for (String time : files) {
            if (time.compareTo(from) < 0 || time.compareTo(to) > 0) {
                continue;
            }
            File file = new File(time + ".dat");
            BufferedReader br = new BufferedReader(new FileReader(file));
            content += time + ":\n";
            String line = br.readLine();
            while (line != null) {
                content += line;
                line = br.readLine();
            }
            content += "\n";
            br.close();
        }

        return content;
    }
}
